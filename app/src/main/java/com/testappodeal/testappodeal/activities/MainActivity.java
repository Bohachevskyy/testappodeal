package com.testappodeal.testappodeal.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.NativeAd;
import com.appodeal.ads.NativeCallbacks;
import com.appodeal.ads.RewardedVideoCallbacks;
import com.appodeal.ads.native_ad.views.NativeAdViewNewsFeed;
import com.testappodeal.testappodeal.R;

import java.util.List;

public class MainActivity extends FragmentActivity {

    NativeAdViewNewsFeed nav_nf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nav_nf = (NativeAdViewNewsFeed) findViewById(R.id.native_ad_view_news_feed);

        Appodeal.setAutoCache(Appodeal.NATIVE, false);
        Appodeal.initialize(this, getString(R.string.appodealKey), Appodeal.NATIVE | Appodeal.REWARDED_VIDEO | Appodeal.BANNER);
        Appodeal.cache(this, Appodeal.NATIVE, 3);

        Appodeal.setNativeCallbacks(new NativeCallbacks() {
            @Override
            public void onNativeLoaded() {
                Log.d("Appodeal", "onNativeLoaded ");

                List<NativeAd> nativeAds = Appodeal.getNativeAds(1);
                if (nativeAds.size() > 0) {
                    nav_nf.setNativeAd(nativeAds.get(0));
                }
            }

            @Override
            public void onNativeFailedToLoad() {
                Log.d("Appodeal", "onNativeFailedToLoad");
            }

            @Override
            public void onNativeShown(NativeAd nativeAd) {
                Log.d("Appodeal", "onNativeShown");
            }

            @Override
            public void onNativeClicked(NativeAd nativeAd) {
                Log.d("Appodeal", "onNativeClicked");
            }
        });


        Appodeal.show(this, Appodeal.BANNER_BOTTOM);
    }

    @Override
    public void onResume() {
        super.onResume();
        Appodeal.onResume(this, Appodeal.NATIVE | Appodeal.REWARDED_VIDEO | Appodeal.BANNER);
    }


    void showRewardedVideo(View v) {
        Appodeal.setRewardedVideoCallbacks(new RewardedVideoCallbacks() {
            @Override
            public void onRewardedVideoLoaded() {
                Log.d("Appodeal", "onRewardedVideoLoaded");
            }

            @Override
            public void onRewardedVideoFailedToLoad() {
                Log.d("Appodeal", "onRewardedVideoFailedToLoad");
            }

            @Override
            public void onRewardedVideoShown() {
                Log.d("Appodeal", "onRewardedVideoShown");
            }

            @Override
            public void onRewardedVideoFinished(int amount, String name) {
                Log.d("Appodeal", String.format("onRewardedVideoFinished. Reward: %d %s", amount, name));
            }

            @Override
            public void onRewardedVideoClosed(boolean finished) {
                Log.d("Appodeal", String.format("onRewardedVideoClosed,  finished: %s", finished));
            }

        });

        Appodeal.show(this, Appodeal.REWARDED_VIDEO);
    }


}
